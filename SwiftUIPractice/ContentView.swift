//
//  ContentView.swift
//  SwiftUIPractice
//
//  Created by Leah Joy Ylaya on 1/19/21.
//

import SwiftUI

struct ContentView: View {
    let columns = [
           GridItem(.flexible()),
           GridItem(.flexible()),
           GridItem(.flexible()),
           GridItem(.flexible())
       ]
    var body: some View {
        VStack {
            Text("Movies").font(.largeTitle)
            ScrollView {
                LazyVGrid(columns: columns) {
                    ForEach(0...100, id: \.self) { _ in
                        Color.orange.frame(width: 100, height: 100)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
